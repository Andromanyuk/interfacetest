//
//  AppDelegate.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/11/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

extension UIApplication {
    
    var statusBar: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
