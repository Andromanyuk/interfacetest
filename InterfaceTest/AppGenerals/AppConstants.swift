//
//  AppConstants.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/15/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import Foundation
import UIKit

struct AppConstants {
    static var scaleFromScreenWidth: CGFloat { return UIScreen.main.bounds.width / 320 }
}
