//
//  ViewController.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/11/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import UIKit
import VisualEffectView

final class ViewController: UIViewController {
    
    // MARK: - Vars&Lets
    private let acters = ["Бен Аффлек","Галь Гадот", "Генри Кавилл", "Джейсон Момоа", "Эзра Миллер", "Рей Фишер", "Джонатан Кимбл Симмонс", "Уиллем Дефо", "Эмбер Хёрд", "Эми Адамс", "Джесси Айзенберг", "Джереми Айронс", "Киаран Хайндс"]
    private var fontSizes = [CGFloat]()
    private var layer: CALayer?
    
    // MARK: - Outlets
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var rateView: UIView!
    @IBOutlet private weak var actersStackView: UIStackView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var mainScrollView: UIScrollView!
    @IBOutlet private weak var filledStarButton: UIBarButtonItem!
    @IBOutlet private weak var emptyStarButton: UIBarButtonItem!
    @IBOutlet private weak var seriesCollectionView: UICollectionView!
    @IBOutlet private weak var mainImage: UIImageView!
    @IBOutlet private weak var possibilityImage: UIImageView!
    @IBOutlet private weak var blurView: VisualEffectView! {
        didSet {
            blurView.blurRadius = 2.3
        }
    }
    @IBOutlet var viewLabels: [UILabel]! {
        didSet {
            fontSizes.removeAll()
            for label in viewLabels {
                fontSizes.append(label.font.pointSize)
            }
        }
    }
    
    
    // MARK - View Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.delegate = self
        seriesCollectionView.delegate = self
        seriesCollectionView.dataSource = self
        
        navigationItem.setRightBarButton(emptyStarButton, animated: false)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        blurView.bounds.size.height = nameLabel.bounds.height + 24
        
        for acter in acters {
            let label = UILabel()
            label.text = acter
            label.alpha = 0.6
            label.font = label.font.withSize(16)
            viewLabels.append(label)
            actersStackView.addArrangedSubview(label)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if layer == nil {
            layer = CALayer()
            updateShadowLayer()
            shadowView.layer.insertSublayer(layer!, at: 0)
        }
    }

    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        seriesCollectionView.performBatchUpdates(nil)
        performScaleFactorUpdate()
        updateShadowLayer()
    }

    // MARK: - Actions
    @IBAction func addToFavorites(_ sender: UIBarButtonItem) {
        navigationItem.setRightBarButton(filledStarButton, animated: true)
    }
    
    @IBAction func removeFromFavorites(_ sender: UIBarButtonItem) {
        navigationItem.setRightBarButton(emptyStarButton, animated: true)
    }
    
    // MARK: - MyTools
    func updateShadowLayer() {
        layer?.shadowRadius = 3 * AppConstants.scaleFromScreenWidth
        layer?.shadowColor = UIColor.black.cgColor
        layer?.shadowOpacity = 0.3
        layer?.shadowOffset = .init(width: 3 * AppConstants.scaleFromScreenWidth, height: 3 * AppConstants.scaleFromScreenWidth)
        layer?.shadowPath = CGPath(roundedRect: possibilityImage.bounds, cornerWidth: possibilityImage.layer.cornerRadius, cornerHeight: possibilityImage.layer.cornerRadius, transform: nil)
    }
    
    func performScaleFactorUpdate() {
        for label in viewLabels {
            label.font = label.font.withSize(AppConstants.scaleFromScreenWidth * fontSizes[viewLabels.firstIndex(of: label)!])
        }
        rateView.layer.cornerRadius = 26 * AppConstants.scaleFromScreenWidth
    }

}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offset = mainScrollView.contentOffset.y / 300
        if offset > 1 {
            offset = 1
        }
        
        let navBarColor = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
        
        navigationController?.navigationBar.tintColor = UIColor(hue: 0.27, saturation: offset, brightness: 0.85, alpha: 1)
        navigationController?.navigationBar.backgroundColor = navBarColor
        UIApplication.shared.statusBar?.backgroundColor = navBarColor
        UIApplication.shared.statusBar?.tintColor = UIColor(hue: 0.27, saturation: offset, brightness: 0.85, alpha: 1)
        navigationController?.navigationBar.barStyle = offset > 0.5 ? .default : .black
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.seriaCell, for: indexPath)!
        cell.seriaImage.image = UIImage(named: "seria\(indexPath.row + 1)")
        cell.seriaLabel.text = "СЕРИЯ \(indexPath.row + 1)"
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight = (collectionView.frame.height)
        return CGSize(width: cellHeight * 1.76, height: cellHeight)
    }
}
