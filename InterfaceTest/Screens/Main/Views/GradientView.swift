//
//  GradientView.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/13/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import UIKit

class GradientView: UIView {

    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.cornerRadius = 5 * AppConstants.scaleFromScreenWidth
    }
}
