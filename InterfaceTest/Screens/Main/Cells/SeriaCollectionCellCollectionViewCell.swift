//
//  SeriaCollectionCellCollectionViewCell.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/12/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import UIKit

final class SeriaCollectionCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var seriaImage: UIImageView!
    @IBOutlet weak var seriaLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        seriaLabel.font = seriaLabel.font.withSize(13 * UIScreen.main.bounds.width / 320)
    }

}
