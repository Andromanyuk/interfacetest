//
//  TimerViewController.swift
//  InterfaceTest
//
//  Created by Андрей Романюк on 4/15/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import UIKit

final class TimerViewController: UIViewController {

    // MARK: - Vars&Lets
    private var fontSizes = [CGFloat]()
    private var startButtonFontSize = CGFloat()
    private var timeInterval = 41047
    private var timer: Timer?
    
    // MARK: - Outlets
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet private var allLabels: [UILabel]!
    @IBOutlet weak var startButton: UIButton!
    
    // MARK: - ViewLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        for label in allLabels {
            fontSizes.append(label.font.pointSize)
        }
        startButtonFontSize = (startButton.titleLabel?.font.pointSize)!
        
        for label in allLabels {
            label.font = label.font.withSize(AppConstants.scaleFromScreenWidth * fontSizes[allLabels.firstIndex(of: label)!])
        }
        startButton.titleLabel?.font = startButton.titleLabel?.font.withSize(AppConstants.scaleFromScreenWidth * startButtonFontSize)
        
        let layer = CALayer()
        layer.shadowRadius = 2
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = .init(width: 0, height: 1)
        layer.shadowPath = CGPath(roundedRect: startButton.frame, cornerWidth: startButton.layer.cornerRadius, cornerHeight: startButton.layer.cornerRadius, transform: nil)
        shadowView.layer.insertSublayer(layer, at: 0)
        
        timer = .scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    // MARK: - Actions
    @IBAction func closeTimerController(_ sender: UIButton) {
        dismiss(animated: true)
        timer?.invalidate()
    }
    
    // MARK: - MyTools
    func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i  :  %02i  :  %02i", hours, minutes, seconds)
    }
    
    @objc func updateTimer() {
        timeInterval -= 1
        timeLabel.text = timeString(time: TimeInterval(timeInterval))
    }
    
}
